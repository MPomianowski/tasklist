package com.pomianowski.tododudu.tododudu.boundry.rest;

import com.pomianowski.tododudu.tododudu.domain.task.Task;
import com.pomianowski.tododudu.tododudu.domain.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskControler {

    @Autowired
    TaskService taskService;

    @RequestMapping("/all")
    public Collection<Task> getAll() {
        return taskService.getAll();
    }

    @PutMapping
    public void add(@RequestBody Task task) {
        taskService.addTask(task);
    }

    @DeleteMapping
    public void delete(@RequestBody int id) {
        taskService.deleteTask(id);
    }

    @PostMapping
    public void change(@RequestBody Task task) {
        taskService.changeTask(task);
    }

}
