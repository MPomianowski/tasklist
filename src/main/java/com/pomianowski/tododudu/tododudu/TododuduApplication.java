package com.pomianowski.tododudu.tododudu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TododuduApplication {

	public static void main(String[] args) {

		SpringApplication.run(TododuduApplication.class, args);
	}

}

