package com.pomianowski.tododudu.tododudu.domain.task;

import lombok.Data;
import java.util.Date;

@Data
public class Task {
    public static int count = 0;
    int id;
    boolean isDone;
    String desc;
    Date creationDate;

    public static int getNewId(){
        count++;
        return count;
    }
}
