package com.pomianowski.tododudu.tododudu.domain.task;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class TaskService {
    Map<Integer, Task> tasks;

    public TaskService() {
        tasks = new HashMap<>();
    }

    public Collection<Task> getAll() {
        return tasks.values();
    }

    public Task addTask(Task task) {
        task.setCreationDate(new Date());
        int newId = Task.getNewId();
        task.setId(newId);
        task.setDone(false);
        tasks.put(newId, task);
        return task;
    }

    public Task changeTask(Task task) {
        int id = task.getId();
        tasks.remove(id);
        task.setCreationDate(new Date());
        task.setId(id);
        tasks.put(id, task);
        return task;
    }

    public void deleteTask(int id) {
        tasks.remove(id);
    }
}
